.. _readme:

FMDeg
============

FMDeg is a tool to identify degrons with bias towards degron-affecting somatic mutations in a dataset of tumor samples. It leverages the DegFI score of each somatic mutation compute the difference between the observed and expected distribution of scores given
the background mutational spectra of the set of samples (mutational spectra of the TCGA cohorts can be found in the Downloads tab). The DegFI score quantifies how much a mutation affects the recognition of a degron by its E3 ubiquitin ligase. DegFI has been pre-computed for all mutations affecting to instances of known degrons (see example directory).  

.. _readme Usage:

Usage
-------


Usage: fmdeg [OPTIONS]

  Run FMDeg on the genomic regions in ELEMENTS FILE using the mutations in
  MUTATIONS FILE.

Options:
  -m, --muts MUTATIONS_FILE       Variants file  [required]
  -e, --elements ELEMENTS_FILE    Genomic elements to analyse  [required]
  -s, --signature PATH            Signature file
  -o, --output OUTPUT_FOLDER      Output folder. Default to regions file name
                                  without extensions.
  -c, --configuration CONFIG_FILE
                                  Configuration file. Default to 'fmdeg.conf'
                                  in the current folder if exists or to
                                  ~/.bbglab/fmdeg.conf if not.
  --debug                         Show more progress details
  --version                       Show the version and exit.
  -h, --help                      Show this message and exit.

.. _readme install:

Formats
-------

The mutation file format must be a tab-separated file (it can be compressed with gzip or not),
with a header and, at least, these fields:

- CHROMOSOME
- POSITION
- REF
- ALT
- SAMPLE


Installation
------------

.. code:: bash

   git clone https://franmj88@bitbucket.org/bbglab/fmdeg.git ./destination_path/

   cd ./destination_path/

   pip install .

.. _readme example:


Run the example
---------------
# Tested in a laptop with 4 cores and 32GB of RAM

.. code:: bash

   cd ./destination_path/run_example_fmdeg/

   fmdeg -m LIHC.tsv.gz -e coordinates_degrons_formatted.tsv.gz -s LIHC.json  -o . -c fmdeg.conf
   
.. _readme license:

License
-------

Apache Software License 2.0


   