
from os import path

from setuptools import Extension
from setuptools import setup, find_packages
from fmdeg import __version__


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()

setup(
    name='fmdeg',
    version=__version__,
    packages=find_packages(),
    package_data={'fmdeg': ['*.txt.gz', '*.conf.template', '*.conf.template.spec', '*.pyx']},
    url="https://bitbucket.org/bbglab/fmdeg",
    download_url="https://bitbucket.org/bbglab/fmdeg/get/"+__version__+".tar.gz",
    license='',
    author='BBGLab (Barcelona Biomedical Genomics Lab)',
    author_email='bbglab@irbbarcelona.org',
    description='',
    setup_requires=[
        'cython',
    ],
    install_requires=required,
    ext_modules=[Extension('fmdeg.walker_cython', ['fmdeg/walker_cython.pyx'])],
    entry_points={
        'console_scripts': [
            'fmdeg = fmdeg.main:cmdline'
        ]
    }
)
