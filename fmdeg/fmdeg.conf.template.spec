reference_genome = string

score_file = string

kmer_size = integer(default=3)

muts_min = integer(default=0)

sampling = integer(default=10000)

sampling_max = integer(default=100000)

sampling_chunk = integer(default=100)

sampling_min_obs = integer(default=0)

cores = integer(default=None)
seed = integer(default=None)
