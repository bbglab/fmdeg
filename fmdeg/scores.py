"""
This module contains the methods associated with the
scores that are assigned to the mutations.

The scores are read from a file.

"""


import logging
from collections import defaultdict, namedtuple
from typing import List

import pandas as pd

from fmdeg import reference


logger = logging.getLogger(__name__)


_FILE = None


def set_file(file):
    """
    Set the file with the scores. Should only be called once
    """
    global _FILE
    _FILE = file
    logging.getLogger(__name__).info('Using %s as scores file', _FILE.upper())


ScoreValue = namedtuple('ScoreValue', ['ref', 'alt', 'value', 'signature'])
"""
Tuple that contains the reference, the alteration, the score value and the triplets

Parameters:
    ref (str): reference base
    alt (str): altered base
    value (float): score value of that substitution
    signature (str): reference kmer sequence and alternate (used with the bgsignature format)
"""


class Scores(object):
    """

    Args:
        element (str): element ID
        segments (list): list of the segments associated to the element
        config (dict): configuration

    Attributes:
        scores_by_pos (dict): for each positions get all possible changes, and for each change the triplets

            .. code-block:: python

                    { position:
                        [
                            ScoreValue(
                                ref,
                                alt_1,
                                value,
                                ref_triplet,
                                alt_triple
                            ),
                            ScoreValue(
                                ref,
                                alt_2,
                                value,
                                ref_triplet,
                                alt_triple
                            ),
                            ScoreValue(
                                ref,
                                alt_3,
                                value,
                                ref_triplet,
                                alt_triple
                            )
                        ]
                    }
    """

    def __init__(self, element, segments, kmer_size):

        self.element = element
        self.segments = segments

        # Scores to load
        self.scores_by_pos = defaultdict(list)

        # Initialize background scores
        self._load_scores(kmer_size)

    def get_score_by_position(self, position: int) -> List[ScoreValue]:
        """
        Get all ScoreValue objects that are asocated with that position

        Args:
            position (int): position

        Returns:
            :obj:`list` of :obj:`ScoreValue`: list of all ScoreValue related to that positon

        """
        return self.scores_by_pos.get(position, [])

    def get_all_positions(self) -> List[int]:
        """
        Get all positions in the element

        Returns:
            :obj:`list` of :obj:`int`: list of positions

        """
        return self.scores_by_pos.keys()

    def _load_scores(self, kmer_size):
        """
        For each position get all possible substitutions and for each
        obtatins the assigned score

        Returns:
            dict: for each positions get a list of ScoreValue
            (see :attr:`scores_by_pos`)
        """
        df = pd.read_csv(_FILE, sep='\t')
        for region in self.segments:
            chromosome = "chr"+region['CHROMOSOME']
            start = region['START']
            end = region['END']
            reg_df = df[(df['chr'] == chromosome) & (df['DEGRON'] == self.element)]
            for pos in range(start, end+1):
                values_at_i = reg_df[reg_df['Coordinate'] == pos]
                try:
                    ref_kmer = reference.get_kmer(region['CHROMOSOME'], pos, kmer_size)
                except ValueError:
                    # ref_triplet = "---"
                    continue
                ref = ref_kmer[kmer_size//2]

                if values_at_i.empty:  # assign 0 to all possible changes (as the file should do)
                    alts = 'ACGT'.replace(ref, '')
                    for a in alts:
                        self.scores_by_pos[pos].append(ScoreValue(ref, a, 0, ref_kmer + '>' + a))
                else:
                    for v in values_at_i.iterrows():  # file should contain all possible changes
                        data = v[1]
                        alt = data['ALT']
                        if alt == ref:
                            continue
                        score = data['SCORE']
                        self.scores_by_pos[pos].append(ScoreValue(ref, alt, score, ref_kmer + '>' + alt))
        del df
