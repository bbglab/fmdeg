"""
Contains the command line parsing and the main class of the method
"""

import io
import os
import sys
import csv
import logging
from os.path import exists
from multiprocessing.pool import Pool
from os import path

import numpy as np
import pandas as pd
import statsmodels.stats.multitest as mt
from bgsignature.file import load as load_signature

from fmdeg import __version__, reference, scores, store
from fmdeg.config import file_exists_or_die, file_name
from fmdeg.executor import ElementExecutor
from fmdeg.load import load_and_map_variants
from fmdeg.utils import executor_run, loop_logging
from fmdeg.walker import flatten_partitions, compute_sampling, partitions_list

logger = logging.getLogger(__name__)


class FMDeg(object):
    """

    Args:
       mutations_file: Mutations input file (see :mod:`~fmdeg.load` for details)
       elements_file: Genomic element input file (see :mod:`~fmdeg.load` for details)
       signature_file: File with the precomputed signatures
       output_folder: Folder where the results will be stored
       config: configuration (see :ref:`configuration <project configuration>`)
       blacklist: File with sample ids (one per line) to remove when loading the input file

    """

    def __init__(self, mutations_file, elements_file, signature_file, output_folder, config):
        logger.debug('Using FMDeg version %s', __version__)

        # Required parameters
        self.mutations_file = file_exists_or_die(mutations_file)
        logger.debug('Mutations file: %s', self.mutations_file)
        self.elements_file = file_exists_or_die(elements_file)
        logger.debug('Elements file: %s', self.elements_file)
        self.signature_file = signature_file
        self.configuration = config

        genome_reference_build = self.configuration['reference_genome']
        reference.set_build(genome_reference_build)

        self.cores = self.configuration['cores']
        if self.cores is None:
            self.cores = os.cpu_count()
        logger.debug('Using %s cores', self.cores)

        # Optional parameters
        self.output_folder = output_folder
        self.output_file_prefix = path.join(self.output_folder, file_name(self.mutations_file) + '-fmdeg')
        logger.debug('Output: %s', self.output_folder)

        self.avoid_parallel = False

        s = io.BytesIO()
        self.configuration.write(s)
        logger.debug('Configuration used:\n' + s.getvalue().decode())
        s.close()

        np.random.seed(self.configuration['seed'])  # Fix an initial random seed

    def run(self):
        """
        Run the FMDeg analysis.
        """

        minimum_mutations = self.configuration['muts_min']
        sampling_min_obs = self.configuration['sampling_min_obs']
        sampling_max = self.configuration['sampling_max']
        sampling_chunk = self.configuration['sampling_max'] * 10**6

        # Load mutations mapping
        mutations, elements = load_and_map_variants(self.mutations_file, self.elements_file, self.configuration['kmer_size'])

        # Load signatures
        if self.signature_file is None:
            signature = None
        else:
            logger.debug('Loading signature')
            signature = load_signature(self.signature_file)

        scores.set_file(self.configuration['score_file'])

        # Create one executor per element
        element_executors = [ElementExecutor(element_id, muts, elements[element_id], signature,
                                             self.configuration, np.random.randint(0, 2**32-1))
                             for element_id, muts in sorted(mutations.items()) if len(muts) >= minimum_mutations]

        # Sort executors to compute first the ones that have more mutations
        element_executors = sorted(element_executors, key=lambda e: -len(e.muts))

        # Run the executors
        with Pool(self.cores) as pool:
            results = {}
            logger.info("Computing FMDeg")
            map_func = map if self.avoid_parallel else pool.imap
            for executor in loop_logging(map_func(executor_run, element_executors), size=len(element_executors), step=6*self.cores):
                if executor.result['nmuts'] >= minimum_mutations:
                    results[executor.name] = executor.result

            # Flatten partitions
            partitions = list(flatten_partitions(results))

            i = 0
            while len(partitions) > 0 or i == 0:

                i += 1
                logger.info("Parallel sampling. Iteration %d, genes %d, partitions %d", i, len(set([n for n,p,r,s in partitions])), len(partitions))

                # Pending sampling execution
                for name, obs, neg_obs in loop_logging(map_func(compute_sampling, partitions), size=len(partitions), step=1):
                    result = results[name]
                    result['obs'] += obs
                    result['neg_obs'] += neg_obs

                # Increase sampling_size
                partitions = []
                for name, result in results.items():
                    sampling_size = float(result['sampling_size'])

                    if result['obs'] is not None and result['obs'] < sampling_min_obs and sampling_size < sampling_max:
                        next_sampling_size = min(sampling_size*10, sampling_max)
                        pending_sampling_size = int(next_sampling_size - sampling_size)
                        chunk_count = (pending_sampling_size * result['nmuts']) // (sampling_chunk)
                        chunk_size = pending_sampling_size if chunk_count == 0 else pending_sampling_size // chunk_count

                        result['sampling_size'] = next_sampling_size
                        for partition in partitions_list(pending_sampling_size, chunk_size):
                            partitions.append((name, partition, result, np.random.randint(0, 2**32-1)))

            # Compute p-values
            logger.info("Compute p-values")
            for result in results.values():
                sampling_size = float(result['sampling_size'])
                result['pvalue'] = max(1, result['obs']) / sampling_size if result['obs'] is not None else None

        if results == {}:
            logger.warning("Empty resutls, possible reason: no mutation from the dataset can be mapped to the provided regions.")
            sys.exit(0)

        # Run multiple test correction
        logger.info("Computing multiple test correction")
        df_results = pd.DataFrame.from_dict(results, orient='index')
        p = df_results["pvalue"].values
        mask = np.isfinite(p)
        pval_corrected = np.full(p.shape, np.nan)
        pval_corrected[mask] = mt.multipletests(p[mask], method='fdr_bh')[1]
        df_results["qvalue"] = pval_corrected

        # Sort and store results
        logger.info("Storing results")
        if not exists(self.output_folder):
            os.makedirs(self.output_folder, exist_ok=True)
        result_file = self.output_file_prefix + '.tsv'
        store.tsv(df_results, result_file)

        # Sanity check
        lines = 0
        gene_ids = {None, ''}
        with open(result_file) as csvfile:
            fd = csv.DictReader(csvfile, delimiter='\t')
            for line in fd:
                lines += 1
                gene_ids.add(line['GENE_ID'])
        if lines+2 != len(gene_ids):
            logger.error('Number of genes does not match number of lines in the output file. Please check the logs of the execution to find more information.')

        logger.info("Creating figures")
        store.png(result_file, self.output_file_prefix + ".png")
        store.html(result_file, self.output_file_prefix + ".html")

        logger.info("Done")
