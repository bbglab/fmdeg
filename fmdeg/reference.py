
import logging

from bgreference import refseq


_BUILD = None


def set_build(build):
    """
    Set the build fo the reference genome. Should only be called once

    Args:
        build (str): genome reference build

    """
    global _BUILD
    _BUILD = build
    logging.getLogger(__name__).info('Using %s as reference genome', _BUILD.upper())


def get_build():
    return _BUILD


def get(chromosome, start, size=1):
    """
    Gets a sequence from the reference genome

    Args:
        chromosome (str): chromosome
        start (int): start position where to look
        size (int): number of bases to retrieve

    Returns:
        str. Sequence from the reference genome

    """
    return refseq(_BUILD, chromosome, start, size)


def get_kmer(chromosome, pos, size):
    """Get a kmer centered in pos"""
    return get(chromosome, pos - size // 2, size=size)
