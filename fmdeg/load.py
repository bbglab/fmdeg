"""
This module contains the methods used to
load and parse the input files: elements and mutations

.. _elements dict:

elements (:obj:`dict`)
    contains all the segments related to one element. The information is taken from
    the :file:`elements_file`.
    Basic structure:

    .. code-block:: python

        { element_id:
            [
                {
                'CHROMOSOME': chromosome,
                'START': start_position_of_the_segment,
                'END': end_position_of_the_segment,
                'STRAND': strand (+ -> positive | - -> negative)
                'ELEMENT': element_id,
                'SEGMENT': segment_id,
                'SYMBOL': symbol_id
                }
            ]
        }


.. _mutations dict:

mutations (:obj:`dict`)
    contains all the mutations for each element. Most of the information is taken from
    the mutations_file but the *element_id* and the *segment* that are taken from the **elements**.
    More information is added during the execution.
    Basic structure:

    .. code-block:: python

        { element_id:
            [
                {
                'CHROMOSOME': chromosome,
                'POSITION': position_where_the_mutation_occurs,
                'REF': reference_sequence,
                'ALT': alteration_sequence,
                'SAMPLE': sample_id,
                'ALT_TYPE': type_of_the_mutation
                }
            ]
        }

"""

import gzip
import logging
import pickle
from collections import defaultdict
from copy import deepcopy

from bgcache import bgcache
from bgparsers import readers
from intervaltree import IntervalTree

from fmdeg import reference


logger = logging.getLogger(__name__)


def load_mutations(file, kmer_size):
    """
    Parsed the mutations file

    Args:
        file: mutations file

    Yields:
        One line from the mutations file as a dictionary. Each of the inner elements of
        :ref:`mutations <mutations dict>`

    """

    logger.info('Loading mutations')

    count = 0
    count_discarded = 0
    count_discarded_ref = 0

    for row in readers.variants(file, required=['CHROMOSOME', 'POSITION', 'REF', 'ALT']):
        count += 1
        if row['CHROMOSOME'] == 'M' or row['ALT_TYPE'] != 'snp':
            count_discarded += 1
            continue
        ref_seq = reference.get_kmer(row['CHROMOSOME'], row['POSITION'], kmer_size)
        if 'N' in ref_seq or ref_seq[kmer_size//2] != row['REF']:
            count_discarded_ref += 1
            continue

        yield row

    discarded = round((count_discarded_ref + count_discarded) * 100 / count, 2)

    if discarded < 25:
        logger.info('Discarded %s %% mutations', discarded)
    elif discarded < 50:
        logger.warning('Discarded %s %% mutations', discarded)
    else:
        logger.warning('Discarded %s %% mutations', discarded)


def build_regions_tree(regions):
    """
    Generates a binary tree with the intervals of the regions

    Args:
        regions (dict): segments grouped by :ref:`elements <elements dict>`.

    Returns:
        dict of :obj:`~intervaltree.IntervalTree`: for each chromosome, it get one :obj:`~intervaltree.IntervalTree` which
        is a binary tree. The leafs are intervals [low limit, high limit) and the value associated with each interval
        is the :obj:`tuple` (element, segment).
        It can be interpreted as:

        .. code-block:: python

            { chromosome:
                (start_position, stop_position +1): (element, segment)
            }

    """
    regions_tree = {}
    for i, (k, allr) in enumerate(regions.items()):

        if i % 7332 == 0:
            logger.info("[%d of %d]", i+1, len(regions))

        for r in allr:
            tree = regions_tree.get(r['CHROMOSOME'], IntervalTree())
            tree[r['START']:(r['END']+1)] = r['ELEMENT']
            regions_tree[r['CHROMOSOME']] = tree

    logger.info("[%d of %d]", i+1, len(regions))
    return regions_tree


@bgcache
def load_elements_tree(elements_file):
    elements = readers.elements_dict(elements_file)
    return build_regions_tree(elements)


def load_and_map_variants(variants_file, elements_file, kmer_size):
    """
    From the elements and variants file, get dictionaries with the segments grouped by element ID and the
    mutations grouped in the same way, as well as some information related to the mutations.

    Args:
        variants_file: mutations file (see :class:`~fmdeg.main.FMDeg`)
        elements_file: elements file (see :class:`~fmdeg.main.FMDeg`)

    Returns:
        tuple: mutations and elements

        Elements: `elements dict`_

        Mutations: `mutations dict`_


    The process is done in 3 steps:
       1. :meth:`load_regions`
       #. :meth:`build_regions_tree`.
       #. each mutation (:meth:`load_mutations`) is associated with the right
          element ID

    """
    logger.info('Loading elements file')
    elements = readers.elements_dict(elements_file)

    # If the input file is a pickle file do nothing
    if variants_file.endswith(".pickle.gz"):
        with gzip.open(variants_file, 'rb') as fd:
            return pickle.load(fd), elements

    logger.info('Building elements tree')
    elements_tree = load_elements_tree(elements_file)

    # Mapping mutations
    variants_dict = defaultdict(list)
    logger.info("Mapping mutations")
    i = 0
    show_small_progress_at = 100000
    show_big_progress_at = 1000000
    for i, r in enumerate(load_mutations(variants_file, kmer_size)):

        if r['CHROMOSOME'] not in elements_tree:
            continue

        if i % show_small_progress_at == 0:
            print('*', end='', flush=True)

        if i % show_big_progress_at == 0:
            print(' [{} muts]'.format(i), flush=True)

        # Get the interval that include that position in the same chromosome
        intervals = elements_tree[r['CHROMOSOME']][r['POSITION']]

        seen_elements = []
        for interval in intervals:
            element = interval.data
            if element not in seen_elements:
                seen_elements.append(element)
                variants_dict[element].append(deepcopy(r))

    if i > show_small_progress_at:
        print('{} [{} muts]'.format(' '*(((show_big_progress_at-(i % show_big_progress_at)) // show_small_progress_at)+1), i), flush=True)

    return variants_dict, elements
