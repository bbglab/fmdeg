"""
Contains the command line parsing
"""

import os
from os import path

import bglogs
import click

from fmdeg import __version__, config
from fmdeg.fmdeg import FMDeg

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def main(mutations_file, elements_file, signature_file, output_folder, config_file):
    """
    Run FMDeg analysis
    """
    output_folder = config.file_name(elements_file) if output_folder is None else output_folder
    output_file = path.join(output_folder, config.file_name(mutations_file) + '-fmdeg.tsv')
    # Skip if done
    if path.exists(output_file):
        bglogs.warning("Already calculated at '{}'".format(output_file))
        return
    else:
        if not path.exists(output_folder):
            os.makedirs(output_folder, exist_ok=True)

    configuration = config.load(config_file)

    analysis = FMDeg(mutations_file, elements_file, signature_file, output_folder, configuration)

    bglogs.info('Running analysis')
    # Run the analysis
    analysis.run()


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-m', '--muts', 'mutations_file', type=click.Path(exists=True), help='Variants file', metavar='MUTATIONS_FILE',required=True)
@click.option('-e', '--elements', 'elements_file', type=click.Path(exists=True), metavar='ELEMENTS_FILE', help='Genomic elements to analyse', required=True)
@click.option('-s', '--signature', 'signature_file', type=click.Path(exists=True), help='Signature file', required=False, default=None)
@click.option('-o', '--output', 'output_folder', type=click.Path(), metavar='OUTPUT_FOLDER', help="Output folder. Default to regions file name without extensions.", default=None)
@click.option('-c', '--configuration', 'config_file', default=None, type=click.Path(exists=True), metavar='CONFIG_FILE', help="Configuration file. Default to 'fmdeg.conf' in the current folder if exists or to ~/.bbglab/fmdeg.conf if not.")
@click.option('--debug', help="Show more progress details", is_flag=True)
@click.version_option(version=__version__)
def cmdline(mutations_file, elements_file, signature_file, output_folder, config_file, debug):
    """
    Run FMDeg on the genomic regions in ELEMENTS FILE
    using the mutations in MUTATIONS FILE.
    """
    bglogs.configure(debug=True if debug else False)

    main(mutations_file, elements_file, signature_file, output_folder, config_file)


if __name__ == "__main__":
    cmdline()
