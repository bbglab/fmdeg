
import logging

import numpy as np

from fmdeg.scores import Scores
from fmdeg.walker import partitions_list

logger = logging.getLogger(__name__)


class ElementExecutor:
    """
    Base class for executors that do the analysis per genomic element.
    The mutations are simulated to compute a p_value
    by comparing the functional impact of the observed mutations
    and the expected.
    The simulation parameters are taken from the configuration file.

    Args:
        element_id (str): element ID
        muts (list): list of mutations belonging to that element (see :ref:`mutations <mutations dict>` inner items)
        segments (list): list of segments belonging to that element (see :ref:`elements <elements dict>` inner items)
        signature (dict): probabilities of each mutation (see :ref:`signatures <signature dict>`)
        config (dict): configurations

    """

    def __init__(self, element_id, muts, segments, signature, config, seed=None):
        # Input attributes
        self.name = element_id
        self.muts = muts
        self.segments = segments
        self.signature = signature

        # Configuration parameters
        self.sampling_size = config['sampling']
        self.min_obs = config['sampling_min_obs']
        self.sampling_chunk = config['sampling_chunk'] * 10**6
        self.kmer_size = config['kmer_size']

        # Output attributes
        symbol = self.segments[0].get('SYMBOL', None)
        self.result = {'symbol': symbol}  # TODO useful ????
        self.scores = None
        self.seed = seed

    def get_score(self, mutation):

        # Get substitutions scores
        mutation['POSITION'] = int(mutation['POSITION'])
        values = self.scores.get_score_by_position(mutation['POSITION'])
        score = None
        for v in values:
            if v.ref == mutation['REF'] and v.alt == mutation['ALT']:
                score = v.value
                break
        return score

    def run(self):
        """
        Loads the scores and compute the statistics for the observed mutations.
        Perform simulations to compute a set of randomized scores.
        By comparing those scores with the observed through different
        statistical tests, a p-value can be obtained.

        """
        np.random.seed(self.seed)

        # Load element scores
        self.scores = Scores(self.name, self.segments, self.kmer_size)

        observed_mutations = []
        observed_scores = []
        observed_positions = set()
        for m in self.muts:
            score = self.get_score(m)
            if score is not None and score != 0:
                observed_mutations.append(m)
                observed_scores.append(score)
                observed_positions.add(m['POSITION'])
        self.result['muts_recurrence'] = len(observed_positions)
        muts_count = len(observed_mutations)
        self.result['nmuts'] = muts_count

        if len(observed_mutations) > 0:
            self.result['observed'] = observed_scores
            observed_mean = np.mean(observed_scores)
            self.result["observed_mean"] = observed_mean

            simulation_scores = []
            simulation_probs = []
            for pos in self.scores.get_all_positions():
                for s in self.scores.get_score_by_position(pos):
                    if s.value == 0:  # get rid of scores 0
                        continue
                    simulation_scores.append(s.value)
                    if self.signature is None:
                        simulation_probs.append(1.0)
                    else:
                        simulation_probs.append(self.signature.get(s.signature, 0.0))

            simulation_scores = np.array(simulation_scores)
            simulation_probs = np.array(simulation_probs)

            if simulation_probs.sum() == 0.0:
                logger.warning('Probability of equal to 0 in {}'.format(self.name))
                # TODO add missing fields
                self.result['partitions'] = []
                self.result['sampling_size'] = self.sampling_size
                self.result['obs'] = None
                self.result['neg_obs'] = None
                return self
            else:
                simulation_probs /= simulation_probs.sum()

            self.result['simulated_mean'] = np.average(simulation_scores, weights=simulation_probs)

            # Calculate sampling parallelization partitions
            chunk_count = (self.sampling_size * muts_count) // self.sampling_chunk
            chunk_size = self.sampling_size if chunk_count == 0 else self.sampling_size // chunk_count
            self.result['partitions'] = partitions_list(self.sampling_size, chunk_size)
            self.result['sampling_size'] = self.sampling_size

            # Run first partition
            first_partition = self.result['partitions'].pop(0)
            background = np.random.choice(simulation_scores, size=(first_partition, muts_count), p=simulation_probs, replace=True)

            values = np.mean(background, axis=1)
            obs = len(values[values >= observed_mean])
            neg_obs = len(values[values <= observed_mean])

            self.result['obs'] = obs
            self.result['neg_obs'] = neg_obs

            # Sampling parallelization (if more than one partition)
            if len(self.result['partitions']) > 0 or obs < self.min_obs:
                self.result['simulation_scores'] = simulation_scores
                self.result['simulation_probs'] = simulation_probs

        return self
