import sys
import glob
import re
directory_input = sys.argv[1]
directory_output = sys.argv[2]
print ("#source activate oncodrivefml_degron")
for file in glob.glob(directory_input+"/mutations_input_*.csv"):

    m = re.search('/mutations_input_([A-Z]+).csv', file)
    if m:
        cancer_type = m.group(1)
        if cancer_type !="PAN":
            print ("fmdeg -m "+file+" -e /workspace/projects/ubiquitins/regions_coordinates_parsed_12aa_isoform_filtered_degrons.csv -o "+directory_output+" -s /workspace/projects/oncodrive/input/datasets/pancanatlas/signatures/"+cancer_type+".signature -c /workspace/users/fmartinez/annotation_ubiquitins/fmdeg/fmdeg/fmdeg/fmdeg.conf")

